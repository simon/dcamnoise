INSTALLATION
============
Installation hints for  Linux/Un*x systems:

INSTALL with: Make install
(gimp-devel-rpm must be installed before, gimptool is needed)
(This is for a rpm-based system, if you use Debian use synaptic or apt-get and search for a
 packet containing gimptool)

It will install in Gimp under "Filter->Verbessern"
(This should be "Filters->Enhance" in the english version)
To uninstall: Make uninstall


SHORT DESCRIPTION AND REQUIREMENTS
==================================================================
This is a gimp plugin to remove typical digital camera noise.

Gimp >= 2.2 is required.

If you use windows, then possibly you cannot compile it.

By courtesy of Michael Schumacher, there is a windows binary for dcamnoise2-0.54.

For windows there also are commercial alternatives:
Look at http://www.imagenomic.com, they have a very nice denoising tool
for windows, in a commercial and in a light free version.
(This is my personal opinion, there are other tools (neatimage,noiseninja....)
Of course they all have disadvantages: They are not gimp-plugins.

Code for dcamnoise2.c is derived from common unsharp-plugin for gimp-2.2.
I used this code as a sceleton, but idea and core code are basing on my own
ideas and work.
It is still very experimental code and a little bit chaotic.
It has undergone a lot of changes.

USAGE
=====

Let me explain, how the filter works, some understanding is necessary to use it:

Hint for the novice user:
In most cases only Filter Max Radius, Filter treshold and Texture Detail are needed and the other
params can be left at their default setting.

Main Filter (Preprocessing)
---------------------------
First, a filtered template is generated, using an adaptive filter.
To see this template, we must set _Luminance tolerance,  _Color tolerance to 1.0.

-----------------------------------------------------------------------------------------------------------

"Filter max. Radius" is preset to 5.0
This is good for most noise situations.
In any case it must be about the same size as noise granularity ore somewhat more.
If it is set higher than necessary, then it can cause unwanted blur.

-----------------------------------------------------------------------------------------------------------

"Filter Threshold" should be set so that edges are clearly visible and noise is  smoothed out.
Adjust it and watch the preview. Adjustment must be made carefully, because the gap
between "noisy", "smooth", and "blur" is very small. Adjust it as carefully as you would adjust
the focus of a camera.

-----------------------------------------------------------------------------------------------------------

"Lookahead" defines the pixel distance in which the filter looks ahead for luminance variations
Normally the default value should do.
When the value is to high, then the adaptive filter cannot longer accurately track image details, and
noise can reappear or blur can occur.

I  had good success with 2.0.
However, for images with extemely high or low resolution another value possibly is better.

-----------------------------------------------------------------------------------------------------------

"Phase Jitter Damping" defines how fast the adaptive filter-radius reacts to luminance variations.
I have preset a value, that should do in most cases.
If increased, then edges appear smoother, if too high, then blur may occur.
If at minimum then noise and phase jitter at edges can occur.
It can supress Spike noise when increased and this is the preferred method to remove spike noise.

-----------------------------------------------------------------------------------------------------------

"Sharpness" does just what it says, it improves sharpness. It improves the frequency response for the filter.
When it is too strong then not all noise can be removed, or spike noise may appear.
Set it near to maximum, if you want to remove weak noise or JPEG-artifacts, without loosing detail.

-----------------------------------------------------------------------------------------------------------

Edge Erosion
If increased then edge noise is removed (eroded) and spike noise is eroded.

-----------------------------------------------------------------------------------------------------------

"Texture Detail" can be used, to get more or less texture accuracy.
When decreased, then noise and texture are blurred out.
It has almost no effect to image edges, opposed to Filter theshold, which would blur edges, when increased.

It can make beautyful skin :-)

-----------------------------------------------------------------------------------------------------------

After this is done the filtered image can be seen seen in the preview.
This image is used as template for the following post-processing processing steps,
therefore it is  important to do these adjustments in first place and to do it as good as possible.

-----------------------------------------------------------------------------------------------------------

.... Combining original image and filtered image, using tolerance thresholds (Postprocessing)..............
This can give a final touch of sharpness to your image.
It is not necessary to do this, if you want to reduce JPEG-artifacts or weak noise.
It's purpose is to master strong noise without loosing too much sharpness.


Adjust _Color tolerance or/and Luminance tolerance, (if necessary) so that you get the final image.
I recommend to use only one, either _Color  or _Luminance.
These settings dont influence the main smoothing process. What they really do is this:

The tolerance values are used as error-thresholds to compare the filtered template with the original
image. The plugin algorithm uses them to combine the filtered template with the original image
so that  noise and filter errors (blur) are thrown out.
A filtered pixel, that differs too much from the original pixel will be gradually replaced by
 original image content.

Hint:
If you cange other sliders, like lookahead or Texture Detail, then you should set color tolerance and
luminance tolerance to 1.0 (right end), because otherwise the filtered template is partially hidden
and e.g. the effects for the damping filter cant be seen clearly and cant be optimized.

-----------------------------------------------------------------------------------------------------------

_Gamma can be used to increase the tolerance values for darker areas (which commonly are more noisy)
This results in more blur for shadow areas.

Keep in mind, how the filter works, then usage should be easy!

============== THANKS ======================================================================================

Thanks go to the gimp-developers for their very fine work!
Thanks to the developer of USM plugin.
I got a lot of positive feedback for this plugin, thanks!



Hacking, Technic & Theory of Operation
--------------------------------------
 Look to:  http://www.hphsite.de/dcamnoise (domain not used anymore)
