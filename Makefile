install: dcamnoise2-0.64.cpp
	export CC=/usr/bin/g++;\
        export CFLAGS="-ffast-math -funroll-loops -O2";\
        gimptool-2.0 --install dcamnoise2-0.64.cpp

uninstall:
	gimptool-2.0 --uninstall-bin dcamnoise2-0.64


